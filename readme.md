######
Apk Tool 
========================
Apk Installer for Windows/Mac OSX/Linux

Windows Screenshot:
========================
![Windows](http://code.h4ck.org.cn/apk-installer-for-windows-mac-osx-linux/raw/ba1091d53d6a447c53fcaddf77c0aa97b0a7d375/Screenshot/win1.0.3.png)

Linux Screenshot:
========================
![Linux](http://code.h4ck.org.cn/apk-installer-for-windows-mac-osx-linux/raw/78e37fa4311b239be48917d3d27dad1b18b94d7c/Screenshot/Linux.png)

Mac OSX Screenshot:
========================
![Mac](http://code.h4ck.org.cn/apk-installer-for-windows-mac-osx-linux/raw/79f58946b88c963e7dfadd87770069b03e4b92ca/Screenshot/Mac1.0.3.png)

Uses:
###
	On Windows:
	Just run the ApkInstaller.py or drag and drop the apk file on the ApkInstaller.py
###
	Linux /Mac OSX:
	1. download the android sdk tool ,and place the aapt and adb into the bin/linux directory on Mac u have to 
		chmod +x aapt and adb
	2. run the command pyhon ApkInstaller.py mas.apk

	Tested on Windows7 /Ubuntu 12.04 /Mac OSX with python 2.7 /PyQt4

To do:
=========================
    1. APK icon select
	2. Binary file auto select and set

Thanks to:
AndroidXMLParser
 AndroidXMLParser was created by Anthony Desnos and is taken from
 the Androguard Project.
 Anthony Desnos <anthony [dot] desnos [at] gmail.com>
 Antitree <antitree [at] gmail.com>
 pee <pee@erkkila.org> spam this
 algorythm <algorythm [at] gmail /dot/ com>