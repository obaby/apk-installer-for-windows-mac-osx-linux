# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 10:34:35 2013
AndroidBridge.py

@author: obaby
"""
import os, platform, subprocess
import hashlib, re

re_packagename = r"""package: name='([^']*)' versionCode='([^']*)' versionName='([^']*)'"""
re_sdkversion = r"""sdkVersion:'(\d+)'"""
re_targetsdkversion = r"""targetSdkVersion:'(\d+)'"""
re_app_label = r"""application-label:'([^']*)'"""

re_app_chs_label = r"""application-label-zh_CN:'([^']*)'"""
re_app_cht_label = r"""application-label-zh_TW:'([^']*)'"""


class ABridge:

    adbPath =''
    aaptPath=''
    
    def __init__(self):
        
        print ("Bridge inited")
        
    def getOsType(self):
        
        systemPlatform = platform.system()  
        if (systemPlatform == "Windows"):
            return "Windows"
        
        if (systemPlatform == "Linux"):
            return "Linux"
        
        if (systemPlatform == "Darwin"):
            return "Darwin"
    
    def initAdbPath(self):
        if (self.getOsType() == "Windows"):
            self.adbPath = os.path.split(os.path.realpath(__file__))[0] + '\\bin\\win\\adb.exe'
            self.adbPath = self.adbPath.replace('\\','\\\\')
        if (self.getOsType() == "Linux"):
            self.adbPath = os.path.split(os.path.realpath(__file__))[0] + '/bin/linux/adb'
            
        if (self.getOsType() == "Darwin"):
            self.adbPath = os.path.split(os.path.realpath(__file__))[0] + '/bin/mac/adb'
        print 'adb is located at : ' + self.adbPath

    def initAaptPath(self):
        if (self.getOsType() == "Windows"):
            self.aaptPath= os.path.split(os.path.realpath(__file__))[0] + '\\bin\\win\\aapt.exe'
            self.aaptPath = self.aaptPath.replace('\\','\\\\')
        if (self.getOsType() == "Linux"):
            self.aaptPath = os.path.split(os.path.realpath(__file__))[0] + '/bin/linux/aapt'
            
        if (self.getOsType() == "Darwin"):
            self.aaptPath = os.path.split(os.path.realpath(__file__))[0] + '/bin/mac/aapt'
        print 'aapt is located at : ' + self.aaptPath
        
    def getDevices(self):
        
        try:
            if (self.getOsType() == "Windows"):
                adbout = subprocess.check_output([self.adbPath, 'devices'],shell=True)
            else:
                adbout = subprocess.check_output([self.adbPath, 'devices'])
        except subprocess.CalledProcessError:
	    print("problem parsing devicelist")
	    return
        
        return adbout
    
    def installApk(self,apkFileName,isInstall2phone):
        
        try:
            if (self.getOsType() == "Windows"):
                if (isInstall2phone == True):
                    adbout = subprocess.check_output([self.adbPath, 'install',apkFileName],shell=True)
                else:
                    adbout = subprocess.check_output([self.adbPath, 'install', '-s',apkFileName],shell=True)
            else:
                if (isInstall2phone == True):
                    adbout = subprocess.check_output([self.adbPath, 'install',apkFileName])
                else:
                    adbout = subprocess.check_output([self.adbPath, 'install', '-s',apkFileName])
        except subprocess.CalledProcessError:
	    print("problem parsing devicelist")
	    return
        return adbout
    
    #def extract_metadata2(self)
        ####GET THE METADATA VIA AAPT.EXE       'C:\\Users\\Administrator\\Desktop\\APKTool\\bin\\win\\aapt.exe' 
    def extract_metadata2(self,apkFileName):
        apk_info = {}
	         
        try:
            if (self.getOsType() == "Windows"):
                adbout = subprocess.check_output([self.aaptPath, 'd', '--values', 'badging', apkFileName],shell=True)
            else:
                adbout = subprocess.check_output([self.aaptPath, 'd', '--values', 'badging', apkFileName])
            
        except subprocess.CalledProcessError:
            print("problem parsing dump data for file ")
            return apk_info
        #@aaptout = subprocess.check_output('ping www.163.com')
        
        print apkFileName
        
        with open(apkFileName, 'rb') as f:
            apk_info['md5'] = hashlib.md5(f.read()).hexdigest()
        m = re.search(re_packagename, adbout)
        if m is not None:
            apk_info['package_name'] = m.group(1)
            apk_info['version_code'] = m.group(2)
            apk_info['version_name'] = m.group(3)
	
        m = re.search(re_sdkversion, adbout)
        if m is not None:
            apk_info['sdk_version'] = m.group(1)
	
        m = re.search(re_targetsdkversion, adbout)
        if m is not None:
            apk_info['target_sdk_version'] = m.group(1)
	
        m = re.search(re_app_label, adbout)
        if m is not None:
            apk_info['app_label'] = m.group(1)

        m = re.search(re_app_chs_label, adbout)
        if m is not None:
            apk_info['app_chs_label'] = m.group(1)

        m = re.search(re_app_cht_label, adbout)
        if m is not None:
            apk_info['app_cht_label'] = m.group(1)

        print apk_info        
        return apk_info
