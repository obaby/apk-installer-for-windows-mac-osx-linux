# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Administrator\Desktop\APKTool\ApkToolUi.ui'
#
# Created: Fri Jul 19 13:32:55 2013
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(466, 280)
        MainWindow.setMinimumSize(QtCore.QSize(466, 280))
        MainWindow.setMaximumSize(QtCore.QSize(466, 280))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 151, 201))
        self.groupBox.setMinimumSize(QtCore.QSize(130, 130))
        self.groupBox.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.graphicsView = QtGui.QGraphicsView(self.groupBox)
        self.graphicsView.setGeometry(QtCore.QRect(6, 20, 0, 0))
        self.graphicsView.setMaximumSize(QtCore.QSize(0, 0))
        self.graphicsView.setObjectName(_fromUtf8("graphicsView"))
        self.groupBox_2 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(163, 10, 301, 201))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.tabWidget = QtGui.QTabWidget(self.groupBox_2)
        self.tabWidget.setGeometry(QtCore.QRect(10, 16, 281, 181))
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.label = QtGui.QLabel(self.tab)
        self.label.setGeometry(QtCore.QRect(10, 10, 54, 12))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.tab)
        self.label_2.setGeometry(QtCore.QRect(10, 40, 54, 12))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.tab)
        self.label_3.setGeometry(QtCore.QRect(10, 70, 54, 12))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.tab)
        self.label_4.setGeometry(QtCore.QRect(10, 100, 54, 12))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_appmd5 = QtGui.QLabel(self.tab)
        self.label_appmd5.setGeometry(QtCore.QRect(80, 10, 181, 16))
        self.label_appmd5.setObjectName(_fromUtf8("label_appmd5"))
        self.label_appapkname = QtGui.QLabel(self.tab)
        self.label_appapkname.setGeometry(QtCore.QRect(80, 40, 181, 16))
        self.label_appapkname.setText(_fromUtf8(""))
        self.label_appapkname.setObjectName(_fromUtf8("label_appapkname"))
        self.label_appversion = QtGui.QLabel(self.tab)
        self.label_appversion.setGeometry(QtCore.QRect(80, 70, 181, 16))
        self.label_appversion.setText(_fromUtf8(""))
        self.label_appversion.setObjectName(_fromUtf8("label_appversion"))
        self.label_appsystemversion = QtGui.QLabel(self.tab)
        self.label_appsystemversion.setGeometry(QtCore.QRect(80, 100, 181, 16))
        self.label_appsystemversion.setText(_fromUtf8(""))
        self.label_appsystemversion.setObjectName(_fromUtf8("label_appsystemversion"))
        self.label_5 = QtGui.QLabel(self.tab)
        self.label_5.setGeometry(QtCore.QRect(10, 130, 54, 12))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.label_appsize = QtGui.QLabel(self.tab)
        self.label_appsize.setGeometry(QtCore.QRect(80, 130, 181, 16))
        self.label_appsize.setText(_fromUtf8(""))
        self.label_appsize.setObjectName(_fromUtf8("label_appsize"))
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.plainTextEdit = QtGui.QPlainTextEdit(self.tab_2)
        self.plainTextEdit.setGeometry(QtCore.QRect(0, 0, 271, 151))
        self.plainTextEdit.setObjectName(_fromUtf8("plainTextEdit"))
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(10, 210, 441, 16))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.pushButton_install = QtGui.QPushButton(self.centralwidget)
        self.pushButton_install.setGeometry(QtCore.QRect(300, 230, 75, 23))
        self.pushButton_install.setObjectName(_fromUtf8("pushButton_install"))
        self.pushButton_remove = QtGui.QPushButton(self.centralwidget)
        self.pushButton_remove.setGeometry(QtCore.QRect(380, 230, 75, 23))
        self.pushButton_remove.setObjectName(_fromUtf8("pushButton_remove"))
        self.checkBox = QtGui.QCheckBox(self.centralwidget)
        self.checkBox.setGeometry(QtCore.QRect(20, 234, 71, 16))
        self.checkBox.setObjectName(_fromUtf8("checkBox"))
        self.label_6 = QtGui.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(170, 230, 40, 16))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.comboBox_dst = QtGui.QComboBox(self.centralwidget)
        self.comboBox_dst.setGeometry(QtCore.QRect(220, 230, 69, 22))
        self.comboBox_dst.setObjectName(_fromUtf8("comboBox_dst"))
        self.comboBox_dst.addItem(_fromUtf8(""))
        self.comboBox_dst.addItem(_fromUtf8(""))
        self.comboBox_devices = QtGui.QComboBox(self.centralwidget)
        self.comboBox_devices.setGeometry(QtCore.QRect(90, 230, 69, 22))
        self.comboBox_devices.setObjectName(_fromUtf8("comboBox_devices"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "APK Tool [火星信息安全研究院]", None))
        self.groupBox.setTitle(_translate("MainWindow", "缩略图", None))
        self.groupBox_2.setTitle(_translate("MainWindow", "软件信息：", None))
        self.label.setText(_translate("MainWindow", "软件MD5：", None))
        self.label_2.setText(_translate("MainWindow", "软件包名：", None))
        self.label_3.setText(_translate("MainWindow", "软件版本：", None))
        self.label_4.setText(_translate("MainWindow", "系统要求：", None))
        self.label_appmd5.setText(_translate("MainWindow", "火星信息安全研究院", None))
        self.label_5.setText(_translate("MainWindow", "软件大小：", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "常规信息", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "权限要求", None))
        self.pushButton_install.setText(_translate("MainWindow", "安装", None))
        self.pushButton_remove.setText(_translate("MainWindow", "卸载", None))
        self.checkBox.setText(_translate("MainWindow", "关联APK", None))
        self.label_6.setText(_translate("MainWindow", "<font color=red>安装到</font>", None))
        self.comboBox_dst.setItemText(0, _translate("MainWindow", "Phone", None))
        self.comboBox_dst.setItemText(1, _translate("MainWindow", "SdCard", None))

