# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 11:10:16 2013

Android APK installer for Windows/Linux/Mac OSX
By obaby QQ:289090351
Blog:http://www.h4ck.org.cn
Email : root@obaby.org.cn
"""
import sys, os, os.path, zipfile,platform
from PyQt4 import QtCore, QtGui
import axmlprinter
from xml.dom import minidom
from xml.etree import ElementTree
import re
from ApkToolUi import Ui_MainWindow
from androidbridge import ABridge

if platform.system()=="Windows":
    import _winreg

appVersion = '1.0.3'

global apkFileName 


class MyApkForm(QtGui.QMainWindow):
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui=Ui_MainWindow()
        self.ui.setupUi(self)
        self.bg=ABridge()

    def file_size_mb(self,filePath): return float(os.path.getsize(filePath)) / (1024 * 1024)

    
    def unzip_file(zipfilename, unziptodir):
        if not os.path.exists(unziptodir): os.mkdir(unziptodir, 0777)
        zfobj = zipfile.ZipFile(zipfilename)
        for name in zfobj.namelist():
            name = name.replace('\\','/')

            if name.endswith('/'):
                os.mkdir(os.path.join(unziptodir, name))
            else:            
                ext_filename = os.path.join(unziptodir, name)
                ext_dir= os.path.dirname(ext_filename)
                if not os.path.exists(ext_dir) : os.mkdir(ext_dir,0777)
                outfile = open(ext_filename, 'wb')
                outfile.write(zfobj.read(name))
                outfile.close()
    
    def convertm_OSVersion2AndroidVersion(self,m_OSVersion):
        return  {
            "2":"Android 1.1",
            "3":"Android 1.5",
            "4":"Android 1.6",
            "5":"Android 2.0",
            "6":"Mars (Not exists)",
            "7":"Android 2.1",
            "8":"Android 2.2",
            "9":"Android 2.3",
            "10":"Android 2.3.3",
            "11":"Android 3.0",
            "12":"Android 3.1",
            "13":"Android 3.2",
            "14":"Android 4.0"
        }.get(m_OSVersion ,"Mars Version")


    def initApkInfo(self,apkFileName):

        if not (zipfile.is_zipfile(apkFileName)) :
            self.ui.statusbar.showMessage("Invalid apk file!")
            #print "Invalid apk file!"
            return 
        apkFile = zipfile.ZipFile(apkFileName)

        permitions = ""
        ap = axmlprinter.AXMLPrinter(apkFile.read("AndroidManifest.xml"))
        buff = minidom.parseString(ap.getBuff()).toxml()
        #print (buff)
        root = ElementTree.fromstring(buff)
        node_findall = root.findall("uses-permission")
        for pers in node_findall :
            permitions = permitions +  pers.get("{http://schemas.android.com/apk/res/android}name") + "\r\n"

        ###AAPT.EXE TO GET THE APP NAME
        apk_info = self.bg.extract_metadata2(apkFileName)
        self.ui.label_appmd5.setText(apk_info.get('md5', '')[:128],)
        #self.ui.label_appname.setText('app_label')
        
        #appapknamenode = root.find("original-package")        
        self.ui.label_appapkname.setText(str(root.get("package")))

        #appversionnode = root.find("manifest")        
        self.ui.label_appversion.setText(str(root.get("{http://schemas.android.com/apk/res/android}versionName")))
        #print (str(root.items()))

        #SYSTEM VERSION REQURIED
        label_appsystemversionnode = root.find("uses-sdk")  
        self.ui.label_appsystemversion.setText(self.convertm_OSVersion2AndroidVersion(label_appsystemversionnode.get("{http://schemas.android.com/apk/res/android}minSdkVersion")))
        self.ui.label_appsize.setText(str(self.file_size_mb(apkFileName)) +" MB")
        self.ui.plainTextEdit.insertPlainText(permitions)
        
        #set mainwindow tittle
        self.setWindowTitle(apk_info.get('app_label', 'Unknown app') + ' --Apk Installer-- ' + appVersion)

        ##set the icon get the largest one        
        
        if ('res/drawable-xhdpi/icon.png' in apkFile.namelist()) :
            targeticon = 'res/drawable-xhdpi/icon.png'
        elif ('res/drawable-hdpi/icon.png' in apkFile.namelist()):
            targeticon = 'res/drawable-hdpi/icon.png'
        elif  ('res/drawable-mdpi/icon.png' in apkFile.namelist()):
            targeticon = 'res/drawable-mdpi/icon.png'
        else :
            targeticon = 'res/drawable-ldpi/icon.png'
            
        pixmp = QtGui.QPixmap()
        pixitem = QtGui.QGraphicsPixmapItem()
        scene = QtGui.QGraphicsScene()
        self.ui.graphicsView = QtGui.QGraphicsView(self.ui.centralwidget)
        grali=[]

            
        pixmp.loadFromData(apkFile.read(targeticon))
        klbild=pixmp.scaledToHeight(100) #with res/drawable-xhdpi/icon.png is 96dip
        pixitem.setPixmap(klbild)
        grali.append(pixitem)
        scene.addItem(grali[-1]) # dirty: no progress, just to have a working code using a  list
        self.ui.graphicsView.setScene(scene)
        self.ui.graphicsView.setGeometry(QtCore.QRect(30, 30, 120, 120))
        self.ui.graphicsView.show()
        #zipfile.close()

    def installApkToPhone(self):
        
        if (self.ui.comboBox_dst.currentText() == "Phone") :
            self.ui.statusbar.showMessage("Install to Phone now.")
            out = self.bg.installApk(apkFileName,True)
            self.ui.statusbar.showMessage(str(out))
        else:
            self.ui.statusbar.showMessage("Install to SdCard now.")
            out = self.bg.installApk(apkFileName,False)
            self.ui.statusbar.showMessage(str(out))

    def detectDevices(self):
        re_device = "([a-f0-9]{15})"
        out = self.bg.getDevices()
        print out
        m = re.search(re_device,out)
        if  (m):
            #print m.group()
            self.ui.comboBox_devices.addItem(m.group())
            self.ui.statusbar.showMessage("Devide " +m.group()+" connected!" )
        else:
            self.ui.statusbar.showMessage("NO device founded,plz plug ur device in 1st!")

    def assocApkFile(self):
        if (platform.system() == "Windows"):
            apk_key = _winreg.OpenKey(_winreg.HKEY_CLASSES_ROOT,r".apk")
            apk_keyvalue ,type = _winreg.QueryValueEx(apk_key,"")
            apk_keyvalue = apk_keyvalue + "\shell\open\command"
            #print apk_keyvalue
            apk_openkey = _winreg.OpenKey(_winreg.HKEY_CLASSES_ROOT, apk_keyvalue)
            apk_openkeyvalue, type = _winreg.QueryValueEx(apk_openkey,"")
	
            apk_backup = _winreg.CreateKey(apk_openkey, "backup")
            _winreg.SetValue(apk_backup, '',type,apk_openkeyvalue)
            print apk_openkeyvalue
    
    @QtCore.pyqtSlot()
    def on_pushButton_install_clicked(self):
	#print "shit"
	MyApkForm.installApkToPhone(self)
		
    @QtCore.pyqtSlot()
    def on_pushButton_remove_clicked(self):
	#print "fuck"
	self.ui.statusbar.showMessage("Remove.")

    @QtCore.pyqtSlot()
    def on_checkBox_clicked(self):
        MyApkForm.assocApkFile(self)
		
if __name__=="__main__":

    app = QtGui.QApplication(sys.argv)
    myapp = MyApkForm()
    #print sys.argv[1]
    myapp.bg.initAdbPath()
    myapp.bg.initAaptPath()    
    if len(sys.argv) >1 :
        apkFileName = sys.argv[1]
        myapp.initApkInfo(apkFileName)
    #myapp.initApkInfo('C:\\Users\\Administrator\\Desktop\\APKTool\\mars.apk')

    myapp.detectDevices()    
    myapp.show()
    sys.exit(app.exec_())
